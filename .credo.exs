%{
  configs: [
    %{
      name: "default",
      files: %{
        included: [
          "lib/magnetis_accounts",
          "lib/magnetis_accounts_web",
          "test/magnetis_accounts",
          "test/magnetis_accounts_web"
        ]
      }
    }
  ]
}

