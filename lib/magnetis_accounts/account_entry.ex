defmodule MagnetisAccounts.AccountEntry do
  @moduledoc """
  Represents an entry in a account, can be a debit or a credit
  """

  use Ecto.Schema
  import Ecto.Changeset

  schema "account_entries" do
    field :account_id, :integer
    field :amount, :float

    timestamps()
  end

  @doc false
  def changeset(account_entry, attrs) do
    account_entry
    |> cast(attrs, [:account_id, :amount])
    |> validate_required([:account_id, :amount])
  end
end
