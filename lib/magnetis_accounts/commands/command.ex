defprotocol MagnetisAccounts.Commands.Command do
  @doc "Executes the command given the params and knowing it is a valid command"
  def execute(command)

  @doc "validates whether the command params are valid or not"
  def validate(command)
end
