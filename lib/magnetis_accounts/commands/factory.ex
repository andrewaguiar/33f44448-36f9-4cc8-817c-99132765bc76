defmodule MagnetisAccounts.Commands.Factory do
  @moduledoc """
  Receives a input value (normally a string comma separated) and creates a command.
  See protocol MagnetisAccounts.Commands.Command
  """

  def create(input_value) do
    input_value
    |> sanitize
    |> create_based_on_tokens(input_value)
  end

  alias MagnetisAccounts.Commands.GetCurrentBalance
  alias MagnetisAccounts.Commands.TransferMoney

  defp sanitize(input_value) do
    input_value
    |> String.trim
    |> String.split(",")
    |> Enum.map(&String.trim/1)
    |> discard_empty_list
  end

  defp discard_empty_list([""]), do: nil
  defp discard_empty_list(list), do: list

  defp create_based_on_tokens([account_id], _) do
    {:ok, %GetCurrentBalance{account_id: account_id}}
  end

  defp create_based_on_tokens([source_account_id, destination_account_id, amount], _) do
    command = %TransferMoney{
      source_account_id: source_account_id,
      destination_account_id: destination_account_id,
      amount: amount
    }

    {:ok, command}
  end

  defp create_based_on_tokens(_, input_value) do
    {:error, "No command found, invalid input: '#{input_value}'"}
  end
end
