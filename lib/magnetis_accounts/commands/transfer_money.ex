defmodule MagnetisAccounts.Commands.TransferMoney do
  @moduledoc """
  The TransferMoney command, used to transfer some amount from a source account to a destination account.

  Input: "<source_account_id>, <destination_account_id>, <amount>"

  Primary course (happy path):
    - Client triggers 'Transfer Money' with the input described above.
    - System validates all data.
    - System creates a debit on the source account.
    - System creates a credit on the destination account.
  Exception Course: Not enough money on the source account.
    - System cancels the transfer.

  See MagnetisAccounts.Commands.TransferMoney.Perform
  """

  defstruct [
    source_account_id: "",
    destination_account_id: "",
    amount: ""
  ]

  defimpl MagnetisAccounts.Commands.Command, for: MagnetisAccounts.Commands.TransferMoney do
    alias MagnetisAccounts.AccountsValidation
    alias MagnetisAccounts.Commands.TransferMoney.Perform

    def validate(command) do
      with :ok <- AccountsValidation.validate_account_id(command.source_account_id, "source_account_id"),
           :ok <- AccountsValidation.validate_account_id(command.destination_account_id, "destination_account_id"),
           :ok <- AccountsValidation.validate_amount(command.amount) do
        :ok
      else
        error -> error
      end
    end

    def execute(command) do
      Perform.call(command)
    end
  end
end
