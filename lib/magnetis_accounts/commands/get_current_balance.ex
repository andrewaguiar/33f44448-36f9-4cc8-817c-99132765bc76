defmodule MagnetisAccounts.Commands.GetCurrentBalance do
  @moduledoc """
  The GetCurrentBalance command, used to get the current balance of a given account.

  Input: "<account_id>"

  Primary course (happy path):
    - Client triggers 'Get balance' with the input described above.
    - System calculates the current balance of the account.
  Exception course: Account does not exist
    - System deliveries a message telling that the account does not exist.
  """

  defstruct [
    account_id: ""
  ]

  defimpl MagnetisAccounts.Commands.Command, for: MagnetisAccounts.Commands.GetCurrentBalance do
    alias MagnetisAccounts.AccountsValidation
    alias MagnetisAccounts.Commands.GetCurrentBalance.Perform

    def validate(%{account_id: account_id}) do
      with :ok <- AccountsValidation.validate_account_id(account_id, "account_id") do
        :ok
      else
        error -> error
      end
    end

    def execute(command) do
      Perform.call(command)
    end
  end
end
