defmodule MagnetisAccounts.Commands.GetCurrentBalance.Perform do
  @moduledoc """
  The GetCurrentBalance execution algorithm, basically we:

  1) checks if account exists.
  2) Sum up all account entries.

  any error in any step will return a Failure with an proper message.
  """

  alias MagnetisAccounts.Account
  alias MagnetisAccounts.Accounts.Balance
  alias MagnetisAccounts.Commands.GetCurrentBalance
  alias MagnetisAccounts.Repo
  alias MagnetisAccounts.Result

  def call(%GetCurrentBalance{account_id: account_id}) do
    case Repo.get(Account, account_id) do
      %Account{id: _} ->
        amount = Balance.calculate(account_id)

        %Result.Success{message: "account #{account_id} has an amount = #{format_amount(amount)}"}
      nil ->
        %Result.Failure{message: "Account #{account_id} not found"}
    end
  end

  defp format_amount(value), do: :erlang.float_to_binary(value, [decimals: 2])
end
