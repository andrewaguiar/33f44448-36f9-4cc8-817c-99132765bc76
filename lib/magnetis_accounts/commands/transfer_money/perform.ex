defmodule MagnetisAccounts.Commands.TransferMoney.Perform do
  @moduledoc """
  The TransferMoney execution algorithm, basically we:

  1) checks if source account exists.
  2) checks if destination account exists.
  3) checks if source account has enough fund.
  4) updates source account decreasing the amount.
  5) updates destination increasing the amount.

  any error in any step will return a Failure with an proper message.
  """

  alias Ecto.Multi
  alias MagnetisAccounts.Account
  alias MagnetisAccounts.AccountEntry
  alias MagnetisAccounts.Accounts.Balance
  alias MagnetisAccounts.Commands.TransferMoney
  alias MagnetisAccounts.Repo
  alias MagnetisAccounts.Result.Failure
  alias MagnetisAccounts.Result.Success

  def call(%TransferMoney{
    source_account_id: string_source_account_id,
    destination_account_id: string_destination_account_id,
    amount: string_amount}) do

    {amount, _} = Float.parse(string_amount)
    {source_account_id, _} = Integer.parse(string_source_account_id)
    {destination_account_id, _} = Integer.parse(string_destination_account_id)

      with :ok <- check_account_exists(source_account_id, "Source account"),
           :ok <- check_account_exists(destination_account_id, "Destination account"),
           :ok <- check_source_account_has_amount(source_account_id, amount) do

      transfer(source_account_id, destination_account_id, amount)
    else
      error -> error
    end
  end

  defp check_account_exists(account_id, account_type) do
    case Repo.get(Account, account_id) do
      nil ->
        %Failure{message: "#{account_type} #{account_id} not found"}
      _account ->
        :ok
    end
  end

  defp check_source_account_has_amount(source_account_id, amount) do
    if Balance.calculate(source_account_id) >= amount do
      :ok
    else
      %Failure{message: "Source account #{source_account_id} does not have funds enough"}
    end
  end

  defp transfer(source_account_id, destination_account_id, amount) do
    # Using multi to execute both update in a transaction
    multi = Multi.new
    |> Multi.insert(:source_account, %AccountEntry{account_id: source_account_id, amount: -amount})
    |> Multi.insert(:destination_account, %AccountEntry{account_id: destination_account_id, amount: amount})

    case Repo.transaction(multi) do
      {:ok, _} ->
        %Success{message: "Transfering done successfuly"}

      {:error, failed_operation, _, _} ->
        %Failure{message: "Error while transfering from #{source_account_id} to #{destination_account_id} caused by #{failed_operation}"}
    end
  end
end
