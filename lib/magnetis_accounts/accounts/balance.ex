defmodule MagnetisAccounts.Accounts.Balance do
  @moduledoc """
  Calculates the balance for a given Account
  """
  alias MagnetisAccounts.Repo

  import Ecto.Query, only: [from: 2]

  def calculate(account_id) do
    query = (
      from ae in "account_entries",
      where: ae.account_id == ^account_id,
      select: ae.amount
    )

    query
    |> Repo.all
    |> Enum.sum
  end
end
