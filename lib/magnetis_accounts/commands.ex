defmodule MagnetisAccounts.Commands do
  @moduledoc """
  Public interface for command execution, receives a string input
  parses it to the proper command (when correct), validates the command
  inputs and executes it.
  """

  alias MagnetisAccounts.Commands.Command
  alias MagnetisAccounts.Commands.Factory

  def execute(input_value) do
    with {:ok, command} <- Factory.create(input_value),
         :ok <- Command.validate(command) do
      Command.execute(command)
    else
      {:error, message} ->
        %MagnetisAccounts.Result.Failure{message: message}
    end
  end
end
