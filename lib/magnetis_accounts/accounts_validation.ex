defmodule MagnetisAccounts.AccountsValidation do
  @moduledoc """
  Provides some Account validation functions
  """

  def validate_account_id(account_id, field_name) do
    case Integer.parse(account_id) do
      {_num, ""} ->
        :ok
      {_num, invalid_chars} ->
        {:error, "invalid chars in #{field_name} '#{invalid_chars}'"}
      :error ->
        {:error, "invalid #{field_name} '#{account_id}'"}
    end
  end

  def validate_amount(amount) do
    case Float.parse(amount) do
      {_num, ""} ->
        :ok
      {_num, invalid_chars} ->
        {:error, "invalid chars in amount '#{invalid_chars}'"}
      :error ->
        {:error, "invalid amount '#{amount}'"}
    end
  end
end
