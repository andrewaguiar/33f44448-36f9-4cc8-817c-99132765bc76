defmodule MagnetisAccounts.Result do
  @moduledoc """
  Represents a result in a command execution, must be either a Success
  or a Failure and must have a message

  See protocol MagnetisAccounts.Commands.Command
  """

  defmodule Success do
    @moduledoc """
    Represents a successfuly result in a command execution

    See protocol MagnetisAccounts.Commands.Command
    """
    defstruct message: ""
  end

  defmodule Failure do
    @moduledoc """
    Represents a fail result in a command execution

    See protocol MagnetisAccounts.Commands.Command
    """
    defstruct message: ""
  end
end
