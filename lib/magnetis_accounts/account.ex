defmodule MagnetisAccounts.Account do
  @moduledoc """
  Represents an account Schema
  """

  alias MagnetisAccounts.Repo

  use Ecto.Schema
  import Ecto.Changeset

  def all do
    Repo.all(__MODULE__)
  end

  schema "accounts" do
    timestamps()
  end

  @doc false
  def changeset(account, attrs) do
    account
    |> cast(attrs, [])
  end
end
