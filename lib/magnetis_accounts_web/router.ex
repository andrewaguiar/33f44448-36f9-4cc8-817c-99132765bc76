defmodule MagnetisAccountsWeb.Router do
  use MagnetisAccountsWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", MagnetisAccountsWeb do
    pipe_through :browser # Use the default browser stack

    get "/", WebConsoleController, :show

    post "/input", InputController, :create
  end

  # Other scopes may use custom stacks.
  # scope "/api", MagnetisAccountsWeb do
  #   pipe_through :api
  # end
end
