defmodule MagnetisAccountsWeb.InputController do
  use MagnetisAccountsWeb, :controller

  alias MagnetisAccounts.Commands
  alias MagnetisAccounts.Result

  def create(conn, %{"value"=> value}) do
    case Commands.execute(value) do
      %Result.Success{message: message} ->
        conn
        |> put_flash(:info, message)
        |> redirect(to: web_console_path(conn, :show))
      %Result.Failure{message: message} ->
        conn
        |> put_flash(:error, message)
        |> redirect(to: web_console_path(conn, :show))
    end
  end
end
