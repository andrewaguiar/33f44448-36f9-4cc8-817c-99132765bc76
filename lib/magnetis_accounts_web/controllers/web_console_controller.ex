defmodule MagnetisAccountsWeb.WebConsoleController do
  use MagnetisAccountsWeb, :controller

  alias MagnetisAccounts.Account

  def show(conn, _params) do
    accounts = Account.all

    conn
    |> assign(:accounts, accounts)
    |> render("show.html")
  end
end
