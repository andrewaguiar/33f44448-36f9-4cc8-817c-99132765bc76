# MagnetisAccounts

## Instaling

```
./setup
```

For more details about the instaling process see `setup` script

## Running in dev

```shell
.run
```

Now you can visit [`localhost:4000/`](http://localhost:4000/) from your browser.

## Running tests

```shell
mix test
```

## Understanding the project

Basically MagnetisAccounts is a web application that simulates a account management system, with
2 features only (for now).

1) Get the Account balance given a account id: `GetCurrentBalance` command.
2) Transfer from a source account to a destination account: `TransferMoney` command.

As a web application we have 2 endpoints, GET `/` that shows a page with a html input to allow us
enter the commands and a POST `/input` that receives the input and executes it.

The process pipelines cn be resumed like:
  1. `InputController` receives the input value (the string command) and calls `Commands.execute`.
  2. `Commands.execute` creates a command struct using the `Commands.Factory`.
  3. The command created by the factory implements the `Command` protocol, so `Commands.execute` calls the `Command.validate` function to validate all command tokens.
  4. After validating if command is ok we call `Command.execute` and the command algorithm is performed.

In order to create another command we need simply to change the `Commands.Factory` to parse the new input to the new `Command` and implement another `defimpl` to that command.

Example:

Lets create a get account number command

1) First we need to change the factory

```elixir
defmodule MagnetisAccounts.Commands.Factory do
  # ...

  defp create_based_on_tokens(["account_number"], _) do
    # this command dont need params
    command = %MagnetisAccounts.Commands.GetAccountNumber{}

    {:ok, command}
  end
```

2) Then we need to create the command end implements the Command protocol. we need to implements the `validate`, and the
`execute` method:

```elixir
defmodule MagnetisAccounts.Commands.GetAccountNumber do
  defstruct []

  defimpl MagnetisAccounts.Commands.Command, for: MagnetisAccounts.Commands.GetAccountNumber do
    def validate(%{account_id: account_id}) do
      :ok
    end

    def execute(command) do
      result = MagnetisAccounts.Account
      |> MagnetisAccounts.Repo.all
      |> length

      %MagnetisAccounts.Result.Success{message: "number of accounts #{result}"}
    end
  end
end
```

In this example as the command is very simple we dont need to treat for any error/exception, sometimes however we need to
verify and return a `MagnetisAccounts.Result.Failure` instead.

I strongly recommend that if the `validate` or/and `execute` method gets complex please, try to extract to another module,
to keep the implementation as easy as possible to understand.
