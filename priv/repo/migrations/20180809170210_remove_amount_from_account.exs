defmodule MagnetisAccounts.Repo.Migrations.RemoveAmountFromAccount do
  use Ecto.Migration

  def change do
    if Mix.env == :prod do
      alter table(:accounts) do
        remove :amount
      end
    else
      # workaround as sqlite don't have drop column
      execute """
        CREATE TABLE accounts_backup AS SELECT id FROM accounts;
        DROP TABLE accounts;
        ALTER TABLE accounts_backup RENAME TO accounts;
      """
    end
  end
end
