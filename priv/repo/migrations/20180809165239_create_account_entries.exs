defmodule MagnetisAccounts.Repo.Migrations.CreateAccountEntries do
  use Ecto.Migration

  def change do
    create table(:account_entries) do
      add :account_id, :integer
      add :amount, :float

      timestamps()
    end
  end
end
