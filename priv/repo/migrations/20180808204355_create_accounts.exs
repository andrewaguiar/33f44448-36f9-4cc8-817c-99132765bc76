defmodule MagnetisAccounts.Repo.Migrations.CreateAccounts do
  use Ecto.Migration

  def change do
    create table(:accounts) do
      add :amount, :float

      timestamps()
    end
  end
end
