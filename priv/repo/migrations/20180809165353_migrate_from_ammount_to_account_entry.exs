defmodule MagnetisAccounts.Repo.Migrations.MigrateFromAmmountToAccountEntry do
  use Ecto.Migration

  alias MagnetisAccounts.Repo
  alias MagnetisAccounts.Account
  alias MagnetisAccounts.AccountEntry

  def change do
    Repo.all(Account)
    |> Enum.map(fn %{id: id, amount: amount} -> %AccountEntry{account_id: id, amount: amount} end)
    |> Enum.each(&Repo.insert!/1)
  end
end
