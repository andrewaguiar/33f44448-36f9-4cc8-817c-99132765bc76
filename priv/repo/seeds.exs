# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     MagnetisAccounts.Repo.insert!(%MagnetisAccounts.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

1..50
|> Enum.each(fn _ ->
  account = MagnetisAccounts.Repo.insert!(%MagnetisAccounts.Account{})

  MagnetisAccounts.Repo.insert!(%MagnetisAccounts.AccountEntry{account_id: account.id, amount: 1500.99})
end)
