defmodule MagnetisAccountsWeb.WebConsoleControllerTest do
  use MagnetisAccountsWeb.ConnCase

  test "GET /", %{conn: conn} do
    conn = get conn, "/"

    assert html_response(conn, 200) =~ "Magnetis Accounts Web Console"
  end
end
