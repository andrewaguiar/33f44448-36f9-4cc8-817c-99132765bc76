defmodule MagnetisAccounts.Commands.FactoryTest do
  use ExUnit.Case

  alias MagnetisAccounts.Commands.Factory
  alias MagnetisAccounts.Commands.GetCurrentBalance
  alias MagnetisAccounts.Commands.TransferMoney

  describe "Factory.create/1" do
    test "when input is a get current balance command" do
      input = " 2423421   "

      expected_command = %GetCurrentBalance{account_id: "2423421"}

      assert Factory.create(input) == {:ok, expected_command}
    end

    test "when input is a transfer money command" do
      input = "2423421, 348957, 100.50"

      expected_command = %TransferMoney{
        source_account_id: "2423421",
        destination_account_id: "348957",
        amount: "100.50"
      }

      assert Factory.create(input) == {:ok, expected_command}
    end

    test "when input is blank" do
      input = ""

      assert Factory.create(input) == {:error, "No command found, invalid input: ''"}
    end

    test "when input is not mapped" do
      input = "2423421,348957,100.50,1000,4"

      assert Factory.create(input) == {:error, "No command found, invalid input: '2423421,348957,100.50,1000,4'"}
    end
  end
end
