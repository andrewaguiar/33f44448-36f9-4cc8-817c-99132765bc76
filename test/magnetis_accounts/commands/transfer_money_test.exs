defmodule MagnetisAccounts.Commands.TransferMoneyTest do
  use ExUnit.Case

  alias Ecto.Adapters.SQL.Sandbox
  alias MagnetisAccounts.Commands.Command
  alias MagnetisAccounts.Commands.TransferMoney

  describe "TransferMoney.validate/1 impl" do
    def validate(source_account_id, destination_account_id, amount) do
      command = %TransferMoney{
        source_account_id: source_account_id,
        destination_account_id: destination_account_id,
        amount: amount
      }

      Command.validate(command)
    end

    test "when account id has a valid account_id" do
      assert validate("123", "456", "120.50") == :ok
    end

    test "when source_account_id invalid" do
      assert validate("123a", "456", "120.20") == {:error, "invalid chars in source_account_id 'a'"}
      assert validate("123.4", "456", "120.20") == {:error, "invalid chars in source_account_id '.4'"}
      assert validate("123,4", "456", "120.20") == {:error, "invalid chars in source_account_id ',4'"}
      assert validate("abc", "456", "120.20") == {:error, "invalid source_account_id 'abc'"}
    end

    test "when destination_account_id invalid" do
      assert validate("123", "456a", "120.20") == {:error, "invalid chars in destination_account_id 'a'"}
      assert validate("123", "456.4", "120.20") == {:error, "invalid chars in destination_account_id '.4'"}
      assert validate("123", "456,4", "120.20") == {:error, "invalid chars in destination_account_id ',4'"}
      assert validate("123", "abc", "120.20") == {:error, "invalid destination_account_id 'abc'"}
    end

    test "when amount invalid" do
      assert validate("123", "456", "120,20") == {:error, "invalid chars in amount ',20'"}
      assert validate("123", "456", "120a20") == {:error, "invalid chars in amount 'a20'"}
      assert validate("123", "456", "abc") == {:error, "invalid amount 'abc'"}
    end
  end

  describe "TransferMoney.execute/1 impl" do
    alias MagnetisAccounts.Account
    alias MagnetisAccounts.AccountEntry
    alias MagnetisAccounts.Repo
    alias MagnetisAccounts.Result.Failure
    alias MagnetisAccounts.Result.Success

    setup do
      :ok = Sandbox.checkout(Repo)

      source_account = %Account{}
      |> Repo.insert!

      Repo.insert!(%AccountEntry{account_id: source_account.id, amount: 1000.0})

      destination_account = %Account{}
      |> Repo.insert!

      Repo.insert!(%AccountEntry{account_id: destination_account.id, amount: 1000.0})

      [source_account: source_account, destination_account: destination_account]
    end

    test "when source account does not exist returns a Failure", context do
      source_account_id = context[:source_account].id + 1000
      destination_account_id = context[:destination_account].id

      command = %TransferMoney{
        source_account_id: "#{source_account_id}",
        destination_account_id: "#{destination_account_id}",
        amount: "100"
      }

      assert Command.execute(command) == %Failure{
        message: "Source account #{source_account_id} not found"
      }
    end

    test "when destination account does not exist returns a Failure", context do
      source_account_id = context[:source_account].id
      destination_account_id = context[:destination_account].id + 1000

      command = %TransferMoney{
        source_account_id: "#{source_account_id}",
        destination_account_id: "#{destination_account_id}",
        amount: "100"
      }

      assert Command.execute(command) == %Failure{
        message: "Destination account #{destination_account_id} not found"
      }
    end

    test "when source account does not have enough funds it returns a Failure", context do
      source_account_id = context[:source_account].id
      destination_account_id = context[:destination_account].id

      command = %TransferMoney{
        source_account_id: "#{source_account_id}",
        destination_account_id: "#{destination_account_id}",
        amount: "2000"
      }

      assert Command.execute(command) == %Failure{
        message: "Source account #{source_account_id} does not have funds enough"
      }
    end

    test "when source account and destination account exists and source account has funds it returns a Success", context do
      source_account_id = context[:source_account].id
      destination_account_id = context[:destination_account].id

      command = %TransferMoney{
        source_account_id: "#{source_account_id}",
        destination_account_id: "#{destination_account_id}",
        amount: "1000"
      }

      assert Command.execute(command) == %Success{
        message: "Transfering done successfuly"
      }
    end
  end
end
