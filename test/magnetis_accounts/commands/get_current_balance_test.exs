defmodule MagnetisAccounts.Commands.GetCurrentBalanceTest do
  use ExUnit.Case

  alias Ecto.Adapters.SQL.Sandbox
  alias MagnetisAccounts.Commands.Command
  alias MagnetisAccounts.Commands.GetCurrentBalance

  describe "GetCurrentBalance.validate/1 impl" do
    def validate(account_id) do
      command = %GetCurrentBalance{
        account_id: account_id,
      }

      Command.validate(command)
    end

    test "when account id has a valid account_id" do
      assert validate("123") == :ok
    end

    test "when account id does not have a valid account_id" do
      assert validate("123a") == {:error, "invalid chars in account_id 'a'"}
      assert validate("123.4") == {:error, "invalid chars in account_id '.4'"}
      assert validate("123,4") == {:error, "invalid chars in account_id ',4'"}
      assert validate("abc") == {:error, "invalid account_id 'abc'"}
    end
  end

  describe "GetCurrentBalance.execute/1 impl" do
    alias MagnetisAccounts.Account
    alias MagnetisAccounts.AccountEntry
    alias MagnetisAccounts.Repo
    alias MagnetisAccounts.Result.Failure
    alias MagnetisAccounts.Result.Success

    setup do
      :ok = Sandbox.checkout(Repo)

      account = %Account{}
      |> Repo.insert!

      Repo.insert!(%AccountEntry{account_id: account.id, amount: 500.0})
      Repo.insert!(%AccountEntry{account_id: account.id, amount: 500.0})
      Repo.insert!(%AccountEntry{account_id: account.id, amount: 500.0})
      Repo.insert!(%AccountEntry{account_id: account.id, amount: -500.0})

      [account: account]
    end

    test "when account does not exist returns a Failure", context do
      account_id = context[:account].id + 100

      command = %GetCurrentBalance{
        account_id: account_id
      }

      assert Command.execute(command) == %Failure{message: "Account #{account_id} not found"}
    end

    test "when account exists returns a Success", context do
      account_id = context[:account].id

      command = %GetCurrentBalance{
        account_id: account_id
      }

      assert Command.execute(command) == %Success{message: "account #{account_id} has an amount = 1000.00"}
    end
  end
end
