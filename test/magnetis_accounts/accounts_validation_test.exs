defmodule MagnetisAccounts.AccountsValidationTest do
  use ExUnit.Case

  alias MagnetisAccounts.AccountsValidation

  describe "Accounts.validate_account_id/1" do
    test "when account id is a perfect integer" do
      assert AccountsValidation.validate_account_id("283947", "account_id") == :ok
    end

    test "when account id has any non integer character" do
      assert AccountsValidation.validate_account_id("2839.47", "account_id") == {:error, "invalid chars in account_id '.47'"}
      assert AccountsValidation.validate_account_id("2839,47", "account_id") == {:error, "invalid chars in account_id ',47'"}
      assert AccountsValidation.validate_account_id("283947A", "account_id") == {:error, "invalid chars in account_id 'A'"}
      assert AccountsValidation.validate_account_id("abc", "account_id") == {:error, "invalid account_id 'abc'"}
    end
  end

  describe "Accounts.validate_amount/1" do
    test "when amount is a perfect float" do
      assert AccountsValidation.validate_amount("120") == :ok
      assert AccountsValidation.validate_amount("120.0") == :ok
    end

    test "when amount has any non float character" do
      assert AccountsValidation.validate_amount("2839,47") == {:error, "invalid chars in amount ',47'"}
      assert AccountsValidation.validate_amount("283947A") == {:error, "invalid chars in amount 'A'"}
      assert AccountsValidation.validate_amount("abc") == {:error, "invalid amount 'abc'"}
    end
  end
end
